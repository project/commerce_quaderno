<?php

namespace Drupal\commerce_quaderno\Form;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\commerce_quaderno\QuadernoInterface;
use Drupal\user\Entity\Role;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configuration form for Quaderno settings.
 */
class ConfigSettingsForm extends ConfigFormBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The Quaderno API.
   *
   * @var \Drupal\commerce_quaderno\QuadernoInterface
   */
  protected $quaderno;

  /**
   * The messenger.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Constructs a ConfigSettingsForm object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   * @param \Drupal\Core\StringTranslation\TranslationInterface $string_translation
   *   The string translation.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\commerce_quaderno\QuadernoInterface $quaderno
   *   The Quaderno API.
   */
  public function __construct(ConfigFactoryInterface $config_factory, MessengerInterface $messenger, ModuleHandlerInterface $module_handler, TranslationInterface $string_translation, EntityTypeManagerInterface $entity_type_manager, QuadernoInterface $quaderno) {
    parent::__construct($config_factory);
    $this->messenger = $messenger;
    $this->moduleHandler = $module_handler;
    $this->stringTranslation = $string_translation;
    $this->entityTypeManager = $entity_type_manager;
    $this->quaderno = $quaderno;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static($container->get('config.factory'), $container->get('messenger'), $container->get('module_handler'), $container->get('string_translation'), $container->get('entity_type.manager'), $container->get('commerce_quaderno.quaderno'));
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'commerce_quaderno_config_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['commerce_quaderno.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('commerce_quaderno.settings');

    $form['configuration'] = [
      '#type' => 'details',
      '#title' => $this->t('Configuration'),
      '#open' => TRUE,
      '#id' => 'configuration-wrapper',
    ];

    $form['configuration']['api_mode'] = [
      '#type' => 'radios',
      '#title' => $this->t('API mode:'),
      '#default_value' => $config->get('api_mode'),
      '#options' => [
        'production' => $this->t('Production'),
        'sandbox' => $this->t('Sandbox'),
      ],
      '#required' => TRUE,
      '#description' => $this->t('The mode to use when calculating taxes. Note: Sandbox mode is available with. <a href="https://sandbox-quadernoapp.com/login">Sandbox Account</a>'),
    ];

    $form['configuration']['api_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API URL'),
      '#default_value' => $config->get('api_url'),
      '#description' => $this->t('Your Quaderno API URL with <strong>trailing slash / (https://example.example-quadernoapp.com/api/)</strong>. <a href="https://developers.quaderno.io/api/?php#introduction">Documentation</a>'),
      '#states' => [
        'required' => [
          ':input[name="api_mode"]' => ['value' => 'production'],
        ],
      ],
    ];

    $form['configuration']['api_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API private key'),
      '#default_value' => $config->get('api_key'),
      '#description' => $this->t('Your Quaderno private key <a href="https://support.quaderno.io/how-do-i-get-my-api-key/">Find your API key</a>. Recommended this value be kept in a settings*.php file and not stored in config. <pre>@example</pre>', [
        '@example' => '$config["commerce_quaderno.settings"]["private_key"] = "";',
      ]),
      '#states' => [
        'required' => [
          ':input[name="api_mode"]' => ['value' => 'production'],
        ],
      ],
    ];

    $form['configuration']['api_version'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API version'),
      '#default_value' => $config->get('api_version'),
      '#description' => $this->t('Your Quaderno API Version. <a href="https://developers.quaderno.io/api/?php#introduction">Documentation</a>.'),
      '#states' => [
        'required' => [
          ':input[name="api_mode"]' => ['value' => 'production'],
        ],
      ],
    ];

    $form['configuration']['sandbox_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Sandbox API URL'),
      '#default_value' => $config->get('sandbox_url'),
      '#description' => $this->t('Enter your sandbox API URL for testing. (https://ninive-ID.sandbox-quadernoapp.com/api/).'),
      '#states' => [
        'visible' => [
          ':input[name="api_mode"]' => ['value' => 'sandbox'],
        ],
        'required' => [
          ':input[name="api_mode"]' => ['value' => 'sandbox'],
        ],
      ],
    ];

    $form['configuration']['sandbox_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Sandbox API private key'),
      '#default_value' => $config->get('sandbox_key'),
      '#description' => $this->t('Enter your sandbox API private key for testing.'),
      '#states' => [
        'visible' => [
          ':input[name="api_mode"]' => ['value' => 'sandbox'],
        ],
        'required' => [
          ':input[name="api_mode"]' => ['value' => 'sandbox'],
        ],
      ],
    ];

    $form['configuration']['sandbox_api_version'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Sandbox API version'),
      '#default_value' => $config->get('sandbox_api_version'),
      '#description' => $this->t('Enter your sandbox API version for testing.'),
      '#states' => [
        'visible' => [
          ':input[name="api_mode"]' => ['value' => 'sandbox'],
        ],
        'required' => [
          ':input[name="api_mode"]' => ['value' => 'sandbox'],
        ],
      ],
    ];

    $form['configuration']['validate'] = [
      '#type' => 'submit',
      '#value' => $this->t('Validate credentials'),
      '#limit_validation_errors' => [],
      '#ajax' => [
        'callback' => [$this, 'validateCredentials'],
        'wrapper' => 'configuration-wrapper',
      ],
    ];

    $form['configuration']['advanced'] = [
      '#type' => 'details',
      '#title' => $this->t('Advanced'),
      '#open' => TRUE,
    ];

    $form['configuration']['advanced']['disable_tax_calculation'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Disable tax calculation'),
      '#description' => $this->t("Enable this option if you don't want to use Quaderno for the tax calculation."),
      '#default_value' => $config->get('disable_tax_calculation'),
    ];

    $form['configuration']['advanced']['enable_reporting'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Use Quaderno for sales tax reporting'),
      '#description' => $this->t('Record order transactions to Quaderno for automated reporting / filing.'),
      '#default_value' => $config->get('enable_reporting'),
      '#states' => [
        'invisible' => [
          ':input[name="disable_tax_calculation"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['configuration']['advanced']['include_discounts'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Include Commerce Promotions'),
      '#description' => $this->t('Include Commerce Promotions/Discounts in the generated invoice.'),
      '#default_value' => $config->get('include_discounts'),
      '#states' => [
        'invisible' => [
          ':input[name="disable_tax_calculation"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['configuration']['advanced']['enable_request_log'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Save invoice requests'),
      '#description' => $this->t('Save invoice requests into order data.'),
      '#default_value' => $config->get('enable_request_log'),
      '#states' => [
        'invisible' => [
          ':input[name="disable_tax_calculation"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['configuration']['advanced']['enable_deliver_invoice'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Send Quadrano invoice to customer'),
      '#description' => $this->t('Use Quadrano API to send generated invoice to the assigned contact email.'),
      '#default_value' => $config->get('enable_deliver_invoice'),
      '#states' => [
        'invisible' => [
          ':input[name="disable_tax_calculation"]' => ['checked' => TRUE],
        ],
      ],
    ];

    // Get Roles.
    $roles = Role::loadMultiple();

    $roles_options = [];

    foreach ($roles as $role_id => $role) {

      if ($role_id == 'anonymous' || $role_id == 'authenticated') {
        continue;
      }

      $roles_options[$role_id] = $role->label();
    }

    $form['configuration']['advanced']['exclude_invoice_roles'] = [
      '#type' => 'checkboxes',
      '#title' => t('Which Roles to exclude from quaderno invoice'),
      '#description' => t('Select which Roles should not receive quaderno invoice.'),
      '#options' => $roles_options,
      '#default_value' => $config->get('exclude_invoice_roles') ?: [],
      '#states' => [
        'invisible' => [
          ':input[name="disable_tax_calculation"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['configuration']['advanced']['logging'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable logging'),
      '#description' => $this->t('Enables detailed Quaderno transaction logging.'),
      '#default_value' => $config->get('logging'),
      '#states' => [
        'invisible' => [
          ':input[name="disable_tax_calculation"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $term_storage = $this->entityTypeManager->getStorage('taxonomy_term');

    $terms = $term_storage->loadByProperties(['vid' => 'quaderno_categories']);

    // Build array of existing categories keyed by Quaderno category code.
    $existing_terms = ['standard' => 'Generally taxable'];
    foreach ($terms as $term) {
      $existing_terms[$term->quaderno_category_code->value] = $term->label();
    }

    $form['configuration']['advanced']['shipping_tax_code'] = [
      '#type' => 'select',
      '#title' => $this->t('Shipping tax code'),
      '#default_value' => $config->get('shipping_tax_code'),
      '#description' => $this->t('The tax code to use for each shipment line item.'),
      '#access' => $this->moduleHandler->moduleExists('commerce_shipping') && !empty($terms),
      '#options' => $existing_terms,
      '#states' => [
        'invisible' => [
          ':input[name="disable_tax_calculation"]' => ['checked' => TRUE],
        ],
        'required' => [
          ':input[name="disable_tax_calculation"]' => ['checked' => FALSE],
        ],
      ],
    ];

    $form['configuration']['advanced']['advanced']['sync_categories'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Sync Product Tax Categories'),
      '#description' => $this->t('Quaderno supplies certain product categories which are used to tag products that are either exempt from sales tax in some jurisdictions or are taxed at reduced rates. These categories are fetched at the time of module installation, and will typically not require updating. If a new category has been added by Quaderno which you would like to use, check this box and submit the form to fetch any updated categories from the Quaderno service.'),
      '#default_value' => empty($terms),
      '#access' => empty($terms),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * Ajax callback for validation.
   */
  public function validateCredentials(array &$form, FormStateInterface $form_state) {
    return $form['configuration'];
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
    $values = $form_state->getValues();
    try {
      $client_factory = \Drupal::service('commerce_quaderno.client_factory');
      $client = $client_factory->createInstance($values);
      $ping_request = $client->get('ping');
      $ping_request = Json::decode($ping_request->getBody()->getContents());
      if (!empty($ping_request) && $ping_request['status'] === 'OK') {
        $this->messenger->addMessage($this->t('Quaderno response confirmed.'));
      }
      else {
        $form_state->setError($form['configuration']['api_key'], $this->t('Could not confirm the provided credentials.'));
        $form_state->setError($form['configuration']['sandbox_key'], $this->t('Could not confirm the provided credentials.'));
      }
    }
    catch (\Exception $e) {
      $form_state->setError($form['configuration'], $e->getMessage());
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $this->config('commerce_quaderno.settings')
      ->set('api_mode', $form_state->getValue('api_mode'))
      ->set('api_key', $form_state->getValue('api_key'))
      ->set('api_url', $form_state->getValue('api_url'))
      ->set('api_version', $form_state->getValue('api_version'))
      ->set('sandbox_key', $form_state->getValue('sandbox_key'))
      ->set('sandbox_url', $form_state->getValue('sandbox_url'))
      ->set('sandbox_api_version', $form_state->getValue('sandbox_api_version'))
      ->set('enable_reporting', $form_state->getValue('enable_reporting'))
      ->set('include_discounts', $form_state->getValue('include_discounts'))
      ->set('enable_request_log', $form_state->getValue('enable_request_log'))
      ->set('enable_deliver_invoice', $form_state->getValue('enable_deliver_invoice'))
      ->set('exclude_invoice_roles', $form_state->getValue('exclude_invoice_roles'))
      ->set('disable_tax_calculation', $form_state->getValue('disable_tax_calculation'))
      ->set('logging', $form_state->getValue('logging'))
      ->set('shipping_tax_code', $form_state->getValue('shipping_tax_code'))
      ->save();

    if ((bool) $form_state->getValue('sync_categories')) {
      $config = $form_state->getValues();
      $this->syncCategories($config);
    }

    parent::submitForm($form, $form_state);
  }

  /**
   * Sync Quaderno categories.
   */
  public function syncCategories($config) {
    try {
      $client_factory = \Drupal::service('commerce_quaderno.client_factory');
      $client = $client_factory->createInstance($config);
      $request = $client->get('tax_codes.json');
      $categories = Json::decode($request->getBody()->getContents());
      if (!empty($categories)) {
        $term_storage = $this->entityTypeManager->getStorage('taxonomy_term');

        $terms = $term_storage->loadByProperties(['vid' => 'quaderno_categories']);

        // Build array of existing categories keyed by Quaderno category code.
        $existing_terms = [];
        foreach ($terms as $term) {
          $existing_terms[$term->quaderno_category_code->value] = $term;
        }

        // Loop through categories, updating or adding as necessary.
        foreach ($categories as $category) {
          if (empty($existing_terms[$category['id']])) {
            $term = $term_storage->create([
              'vid' => 'quaderno_categories',
            ]);
          }
          else {
            $term = $existing_terms[$category['id']];
          }
          $term->set('name', $category['name']);
          $term->set('quaderno_category_code', $category['id']);
          $term->set('description', $category['description']);
          $term->save();
        }
        $this->messenger()
          ->addMessage($this->t('Quaderno product categories updated.'));
      }
      else {
        $this->messenger()
          ->addWarning($this->t('Quaderno product categories could not be updated.'));
      }
    }
    catch (\Exception $e) {
      $this->messenger()
        ->addWarning($e->getMessage());
    }
  }

}
