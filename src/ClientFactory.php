<?php

namespace Drupal\commerce_quaderno;

use Drupal\Core\Http\ClientFactory as CoreClientFactory;

/**
 * API Client factory.
 */
class ClientFactory {

  /**
   * ClientFactory object.
   *
   * @var \Drupal\commerce_quaderno\ClientFactory
   */
  protected $clientFactory;

  /**
   * Constructs a new Quaderno ClientFactory object.
   *
   * @param \Drupal\Core\Http\ClientFactory $client_factory
   *   The client factory.
   */
  public function __construct(CoreClientFactory $client_factory) {
    $this->clientFactory = $client_factory;
  }

  /**
   * Gets an API client instance.
   *
   * @param array $config
   *   The config for the client.
   *
   * @return \GuzzleHttp\Client
   *   The API client.
   */
  public function createInstance(array $config) {
    $options = [];
    if ($config) {
      switch ($config['api_mode']) {
        case 'production':
          $base_uri = $config['api_url'];
          $api_key = $config['api_key'];
          $api_version = $config['api_version'];
          break;

        case 'development':
        default:
          $base_uri = $config['sandbox_url'];
          $api_key = $config['sandbox_key'];
          $api_version = $config['sandbox_api_version'];
          break;
      }

      $options = [
        'base_uri' => $base_uri,
        'auth' => [$api_key, 'x'],
        'headers' => [
          'Accept' => 'application/json; api_version=' . $api_version,
          'Content-Type' => 'application/json',
        ],
      ];
    }

    return $this->clientFactory->fromOptions($options);
  }

}
