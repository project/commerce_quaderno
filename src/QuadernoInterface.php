<?php

namespace Drupal\commerce_quaderno;

/**
 * Provides an interface for the Quaderno API.
 *
 * For more information please visit
 * https://developers.quaderno.io/api/?php#introduction .
 */
interface QuadernoInterface {

  /**
   * Validate credentials (GET /ping).
   *
   * @return mixed
   *   The response array.
   */
  public function ping();

  /**
   * Tax Codes (GET /tax_codes).
   *
   * @return array
   *   The response array.
   */
  public function getTaxCodes(): array;

  /**
   * Calculate a tax rate (GET /tax_rates/calculate).
   *
   * @param array $request_body
   *   The formated request.
   *
   * @return array
   *   The response array.
   */
  public function calculateTax(array $request_body): array;

  /**
   * Create an invoice (POST /invoices).
   *
   * @param array $request_body
   *   The formated request.
   *
   * @return array
   *   The response array.
   */
  public function createInvoice(array $request_body): array;

  /**
   * Deliver an invoice (GET /invoices/INVOICE_ID/deliver).
   *
   * @param string $invoice_id
   *   The invoice identifier.
   *
   * @return mixed
   *   The response.
   */
  public function deliverInvoice(string $invoice_id);

  /**
   * Delete an invoice (DELETE /invoices/INVOICE_ID).
   *
   * @param string $invoice_id
   *   The invoice identifier.
   *
   * @return mixed
   *   The response.
   */
  public function deleteInvoice(string $invoice_id);

  /**
   * Create an evidence (POST /evidence.json).
   *
   * @param array $request_body
   *   The formated request.
   *
   * @return array
   *   The response array.
   */
  public function createEvidence(array $request_body): array;

  /**
   * Create a credit (POST /credits).
   *
   * @param string $invoice_id
   *   The invoice identifier.
   *
   * @return array
   *   The response array.
   */
  public function createCredit(string $invoice_id): array;

  /**
   * Deliver credit email (GET /credits/CREDIT_ID/deliver).
   *
   * @param string $credit_id
   *   The credit identifier.
   *
   * @return mixed
   *   The response.
   */
  public function deliverCredit(string $credit_id);

  /**
   * Delete credit (DELETE /credits/CREDIT_ID).
   *
   * @param string $credit_id
   *   The credit identifier.
   *
   * @return mixed
   *   The response.
   */
  public function deleteCredit(string $credit_id);

}
