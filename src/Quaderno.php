<?php

namespace Drupal\commerce_quaderno;

use Drupal\commerce_order\AdjustmentTypeManager;
use Drupal\Component\Serialization\Json;
use Drupal\Component\Utility\Variable;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * The Quaderno API integration.
 */
class Quaderno implements QuadernoInterface {

  /**
   * The adjustment type manager.
   *
   * @var \Drupal\commerce_order\AdjustmentTypeManager
   */
  protected $adjustmentTypeManager;

  /**
   * The client.
   *
   * @var \GuzzleHttp\Client
   */
  protected $client;

  /**
   * The Quaderno configuration.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * The event dispatcher.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * The logger.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Constructs a new Quaderno object.
   *
   * @param \Drupal\commerce_order\AdjustmentTypeManager $adjustment_type_manager
   *   The chain tax code resolver.
   * @param \Drupal\commerce_quaderno\ClientFactory $client_factory
   *   The client.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The configuration factory.
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $event_dispatcher
   *   The event dispatcher.
   * @param \Psr\Log\LoggerInterface $logger
   *   The logger.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   */
  public function __construct(AdjustmentTypeManager $adjustment_type_manager, ClientFactory $client_factory, ConfigFactoryInterface $config_factory, EventDispatcherInterface $event_dispatcher, LoggerInterface $logger, ModuleHandlerInterface $module_handler) {
    $this->adjustmentTypeManager = $adjustment_type_manager;
    $this->config = $config_factory->get('commerce_quaderno.settings');
    $this->client = $client_factory->createInstance($this->config->get());
    $this->eventDispatcher = $event_dispatcher;
    $this->logger = $logger;
    $this->moduleHandler = $module_handler;
  }

  /**
   * {@inheritdoc}
   */
  public function ping() {
    return $this->doRequest('GET', 'ping');
  }

  /**
   * {@inheritdoc}
   */
  public function getTaxCodes(): array {
    return $this->doRequest('GET', 'tax_codes.json');
  }

  /**
   * {@inheritdoc}
   */
  public function calculateTax(array $request_body): array {
    $response_body = [];

    foreach ($request_body['line_items'] as $line_item) {
      $amount = $line_item['amount'];
      if (!empty($line_item['discount_rate']) && $line_item['discount_rate'] !== 0) {
        $amount = $line_item['discounted_unit_price'];
      }

      $result = $this->doRequest('GET', 'tax_rates/calculate', [
        'query' => [
          'to_country' => $line_item['to_country'],
          'to_postal_code' => $line_item['to_postal_code'],
          'from_country' => $request_body['from_country'],
          'from_postal_code' => $request_body['from_postal_code'],
          'amount' => $amount,
          'tax_behavior' => $line_item['tax_behavior'],
          'tax_code' => $line_item['product_tax_code'] ?? '',
          'tax_id' => $line_item['tax_id'] ?? '',
        ],
      ]);
      $result['uuid'] = $line_item['uuid'];
      $response_body['items_attributes'][] = $result;
    }

    return $response_body;
  }

  /**
   * {@inheritdoc}
   */
  public function createInvoice($request_body): array {
    return $this->doRequest('POST', 'invoices.json', ['json' => $request_body]);
  }

  /**
   * {@inheritdoc}
   */
  public function deliverInvoice($invoice_id) {
    return $this->doRequest('GET', 'invoices/' . $invoice_id . '/deliver.json');
  }

  /**
   * {@inheritdoc}
   */
  public function deleteInvoice($invoice_id) {
    return $this->doRequest('DELETE', 'invoices/' . $invoice_id . '.json');
  }

  /**
   * {@inheritdoc}
   */
  public function createEvidence($request_body): array {
    return $this->doRequest('POST', 'evidence.json', ['json' => $request_body]);
  }

  /**
   * {@inheritdoc}
   */
  public function createCredit($invoice_id): array {
    return $this->doRequest('POST', 'credits.json', [
      'form_params' => [
        'invoice_id' => $invoice_id,
      ],
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function deliverCredit($credit_id) {
    return $this->doRequest('GET', 'credits/' . $credit_id . '/deliver.json');
  }

  /**
   * {@inheritdoc}
   */
  public function deleteCredit($credit_id) {
    return $this->doRequest('DELETE', 'credits/' . $credit_id . '.json');
  }

  /**
   * Performs an HTTP request to Quaderno.
   *
   * @param string $method
   *   The HTTP method to use.
   * @param string $path
   *   The remote path. The base URL will be automatically appended.
   * @param array $parameters
   *   An array of fields to include with the request. Optional.
   *
   * @return array
   *   The response array.
   */
  protected function doRequest($method, $path, array $parameters = []) {
    $response_body = [];
    try {
      $response = $this->client->request($method, $path, $parameters);
      $response_body = Json::decode($response->getBody()->getContents());
    }
    catch (ClientException $e) {
      $response_body = Json::decode($e->getResponse()
        ->getBody()
        ->getContents());
      $this->logger->error($e->getResponse()->getBody()->getContents());
    }
    catch (\Exception $e) {
      $this->logger->error($e->getMessage());
    }
    // Log the response/request if logging is enabled.
    if ($this->config->get('logging')) {
      $url = $this->client->getConfig('base_uri') . $path;
      $this->logger->info("URL: <pre>$method @url</pre>Headers: <pre>@headers</pre>Request: <pre>@request</pre>Response: <pre>@response</pre>", [
        '@url' => $url,
        '@headers' => Variable::export($this->client->getConfig('headers')),
        '@request' => Variable::export($parameters),
        '@response' => Variable::export($response_body),
      ]);
    }

    return $response_body;
  }

  /**
   * Sets the http client.
   *
   * @param \GuzzleHttp\Client $client
   *   The http client.
   *
   * @return $this
   */
  public function setClient(Client $client) {
    $this->client = $client;
    return $this;
  }

}
