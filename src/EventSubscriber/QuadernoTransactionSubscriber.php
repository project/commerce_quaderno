<?php

namespace Drupal\commerce_quaderno\EventSubscriber;

use Drupal\commerce_order\Event\OrderEvent;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\state_machine\Event\WorkflowTransitionEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Event subscriber to handle syncing order data with Quaderno.
 */
class QuadernoTransactionSubscriber implements EventSubscriberInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The Quaderno configuration.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * Constructs a new CommitTransactionSubscriber object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The configuration factory.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, ConfigFactoryInterface $config_factory) {
    $this->entityTypeManager = $entity_type_manager;
    $this->config = $config_factory->get('commerce_quaderno.settings');
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events = [
      'commerce_order.commerce_order.presave' => ['updateTransaction'],
      'commerce_payment.refund.post_transition' => ['refundTransaction'],
      'commerce_order.commerce_order.delete' => ['deleteTransaction'],
    ];
    return $events;
  }

  /**
   * Creates a transaction in Quaderno.
   *
   * @param \Drupal\commerce_order\Event\OrderEvent $event
   *   The order update event.
   */
  public function updateTransaction(OrderEvent $event) {
    /** @var \Drupal\commerce_order\Entity\OrderInterface $order */
    $order = $event->getOrder();

    $quaderno_data = $order->getData('quaderno');

    if (!empty($quaderno_data)) {
      $tax_type = $this->entityTypeManager->getStorage('commerce_tax_type')
        ->load($quaderno_data['plugin_id']);
      $plugin = $tax_type->getPlugin();
      if ($this->config->get('enable_reporting') && !empty($order->original)) {
        // Order created.
        if ($order->original->getState()->value === 'draft' && $order->getState()->value !== 'draft') {
          $plugin->createTransaction($order);
        }
      }
    }
  }

  /**
   * Refunds a transaction in Quaderno.
   *
   * @param \Drupal\state_machine\Event\WorkflowTransitionEvent $event
   *   The workflow transition event.
   */
  public function refundTransaction(WorkflowTransitionEvent $event) {
    $payment = $event->getEntity();
    $order = $payment->getOrder();

    $quaderno_data = $order->getData('quaderno');

    if (!empty($quaderno_data)) {
      $tax_type = $this->entityTypeManager->getStorage('commerce_tax_type')
        ->load($quaderno_data['plugin_id']);
      $plugin = $tax_type->getPlugin();
      if ($this->config->get('enable_reporting')) {
        $plugin->refundTransaction($order);
      }
    }
  }

  /**
   * Deletes a transaction in Quaderno.
   *
   * @param \Drupal\commerce_order\Event\OrderEvent $event
   *   The order delete event.
   */
  public function deleteTransaction(OrderEvent $event) {
    $order = $event->getOrder();

    $quaderno_data = $order->getData('quaderno');

    if (!empty($quaderno_data)) {
      $tax_type = $this->entityTypeManager->getStorage('commerce_tax_type')
        ->load($quaderno_data['plugin_id']);
      $plugin = $tax_type->getPlugin();

      if ($this->config->get('enable_reporting')) {
        $plugin->deleteTransaction($order);
      }
    }
  }

}
