<?php

namespace Drupal\commerce_quaderno\Plugin\views\field;

use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Displays the Quaderno Invoice permalink and pdf.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("quaderno_invoice_links")
 */
class QuadernoInvoiceLinks extends FieldPluginBase {

  /**
   * Constructs a Quaderno Invoice object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition
    );
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    // Do nothing.
  }

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values) {

    $data_entity = $this->getEntity($values);
    if(!is_null($data_entity) && $data_entity->hasField('data')){
      $order_data = $data_entity->get('data')->getValue()[0];
    } else {
      return 'This field only works on Order views';
    }

    if (!empty($order_data['quaderno']) && !empty($order_data['quaderno']['transactionInvoice'])) {
      $invoice_url = $order_data['quaderno']['transactionInvoice']['permalink'];
      $invoice_pdf_url = $order_data['quaderno']['transactionInvoice']['pdf'];
      $output = [
        '#theme' => 'commerce_quaderno_invoice_links',
        '#invoice_url' => $invoice_url,
        '#invoice_pdf_url' => $invoice_pdf_url,
      ];
      return $output;
    }

    return '';
  }

}
