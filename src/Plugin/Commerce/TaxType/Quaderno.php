<?php

namespace Drupal\commerce_quaderno\Plugin\Commerce\TaxType;

use Drupal\commerce_quaderno\QuadernoInterface;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_order\Adjustment;
use Drupal\commerce_price\Price;
use Drupal\commerce_tax\Plugin\Commerce\TaxType\RemoteTaxTypeBase;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Session\AccountProxy;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Provides the Quaderno remote tax type.
 *
 * @CommerceTaxType(
 *   id = "quaderno",
 *   label = "Quaderno",
 * )
 */
class Quaderno extends RemoteTaxTypeBase {

  /**
   * Drupal\Core\Session\AccountProxy definition.
   *
   * @var \Drupal\Core\Session\AccountProxy
   */
  protected $currentUser;


  /**
   * The Quaderno API.
   *
   * @var \Drupal\commerce_quaderno\QuadernoInterface
   */
  protected $quaderno;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The logger.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * The messenger.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The Quaderno config object.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $config;

  /**
   * The corresponding request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * Constructs a new Quaderno object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $event_dispatcher
   *   The event dispatcher.
   * @param \Drupal\commerce_quaderno\QuadernoInterface $quaderno
   *   The Quaderno API.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   The logger.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The corresponding request stack.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, EventDispatcherInterface $event_dispatcher, AccountProxy $current_user, QuadernoInterface $quaderno, ModuleHandlerInterface $module_handler, LoggerChannelFactoryInterface $logger_factory, MessengerInterface $messenger, ConfigFactoryInterface $config_factory, RequestStack $request_stack) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $entity_type_manager, $event_dispatcher);
    $this->currentUser = $current_user;
    $this->quaderno = $quaderno;
    $this->moduleHandler = $module_handler;
    $this->logger = $logger_factory->get('commerce_quaderno');
    $this->messenger = $messenger;
    $this->configFactory = $config_factory;
    $this->config = $this->configFactory->get('commerce_quaderno.settings');
    $this->requestStack = $request_stack;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static($configuration, $plugin_id, $plugin_definition, $container->get('entity_type.manager'), $container->get('event_dispatcher'), $container->get('current_user'), $container->get('commerce_quaderno.quaderno'), $container->get('module_handler'), $container->get('logger.factory'), $container->get('messenger'), $container->get('config.factory'), $container->get('request_stack'));
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'display_inclusive' => FALSE,
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function applies(OrderInterface $order) {
    return !$this->config->get('disable_tax_calculation');
  }

  /**
   * {@inheritdoc}
   */
  public function apply(OrderInterface $order) {
    $order_data = $order->getData($this->pluginId) ?? [];
    $request_body = $this->buildRequest($order);

    if (empty($request_body['line_items'])) {
      return;
    }

    if (!empty($order_data['request']) && !empty($order_data['taxResponse']) && $order_data['taxRequest']['lines'] == $request_body['lines']) {
      $response_body = $order_data['response'];
    }
    else {
      $response_body = $this->quaderno->calculateTax($request_body);
    }

    $adjustments = [];
    $applied_adjustments = [];
    if (!empty($response_body)) {
      foreach ($response_body['items_attributes'] as $item) {
        $adjustments[$item['uuid']] = $item;
      }
    }

    $currency_code = $order->getTotalPrice() ? $order->getTotalPrice()
      ->getCurrencyCode() : $order->getStore()->getDefaultCurrencyCode();

    foreach ($order->getItems() as $order_item) {
      if (!isset($adjustments[$order_item->uuid()]['tax_amount'])) {
        continue;
      }
      $tax_amount = new Price((string) $adjustments[$order_item->uuid()]['tax_amount'], $currency_code);

      // Now determine the tax amount, taking into account other adjustments.
      $order_item->addAdjustment(new Adjustment([
        'type' => 'tax',
        'label' => $adjustments[$order_item->uuid()]['name'] ?? 'Sales tax',
        'amount' => $tax_amount,
        'percentage' => (string) ($adjustments[$order_item->uuid()]['rate'] / 100),
        'source_id' => $this->pluginId . '|' . $this->parentEntity->id(),
        'included' => $request_body['prices_include_tax'],
      ]));
      $applied_adjustments[$order_item->uuid()] = $order_item->uuid();
    }

    // If we still have Tax adjustments to apply, add a single one to the order.
    $remaining_adjustments = array_diff_key($adjustments, $applied_adjustments);
    if (!$remaining_adjustments) {
      return;
    }
    $tax_adjustment_total = NULL;

    // Calculate the total Tax adjustment to add.
    foreach ($remaining_adjustments as $remaining_adjustment) {
      if (!isset($remaining_adjustment['tax_amount'])) {
        continue;
      }
      $adjustment_amount = new Price((string) $remaining_adjustment['tax_amount'], $currency_code);
      $tax_adjustment_total = $tax_adjustment_total ? $tax_adjustment_total->add($adjustment_amount) : $adjustment_amount;
    }

    if (!is_null($tax_adjustment_total)) {
      $order->addAdjustment(new Adjustment([
        'type' => 'tax',
        'label' => 'Sales tax',
        'amount' => $tax_adjustment_total,
        'source_id' => $this->pluginId . '|' . $this->parentEntity->id(),
        'included' => $request_body['prices_include_tax'],
      ]));
    }

    $order_data['plugin_id'] = $this->parentEntity->id();
    $order_data['taxRequest'] = $request_body;
    $order_data['taxResponse'] = $response_body;

    // Store the Quaderno data in the order.
    $order->setData($this->pluginId, $order_data);

  }

  /**
   * Make create transaction request to API.
   */
  public function createTransaction(OrderInterface $order) {
    $order_data = $order->getData($this->pluginId);

    $request_body = $this->buildRequest($order, 'transaction');
    $order_data['transactionRequest'] = $request_body;

    // Create an invoice.
    // https://developers.quaderno.io/api/?php#invoices .
    $response_body = $this->quaderno->createInvoice($request_body);

    if ($this->config->get('enable_reporting') == TRUE) {
      $order_data['transactionInvoice'] = $response_body;
    }
    else {
      $order_data['transactionInvoice']['id'] = $response_body['id'];
    }
    $order->setData($this->pluginId, $order_data);

    // Create an evidence.
    // https://developers.quaderno.io/api/?php#evidence .
    if (!empty($response_body['id'])) {
      $e_request_body = [
        'document_id' => $response_body['id'],
        'billing_country' => $request_body['contact']['country'],
        'bank_country' => $request_body['contact']['country'],
        'ip_address' => $request_body['custom_metadata']['ip_address'],
      ];
      $this->quaderno->createEvidence($e_request_body);
    }

    // Deliver an invoice.
    // https://developers.quaderno.io/api/?php#deliver-an-invoice .
    if (!empty($response_body['id']) && $this->config->get('enable_deliver_invoice') == TRUE) {
      $roles = $this->config->get('exclude_invoice_roles');
      $user_roles = $this->currentUser->getRoles();
      $send_email = TRUE;

      if (!empty($roles)) {
        foreach ($roles as $role){
          foreach ($user_roles as $user_role){
             if ($role === $user_role){
               $send_email = FALSE;
             }
          }
        }
      }

      if($send_email == TRUE){
        $this->quaderno->deliverInvoice($response_body['id']);
      }
    }
  }

  /**
   * Make refund transaction request to API.
   */
  public function refundTransaction(OrderInterface $order) {
    $order_data = $order->getData($this->pluginId);

    if (!empty($order_data['transactionInvoice']['id'])) {

      // Create a credit.
      // https://developers.quaderno.io/api/?php#create-a-credit .
      $response_body = $this->quaderno->createCredit($order_data['transactionInvoice']['id']);

      if ($this->config->get('enable_reporting') == TRUE) {
        $order_data['transactionCredit'] = $response_body;
      }
      else {
        $order_data['transactionCredit']['id'] = $response_body['id'];
      }

      // Deliver an credit.
      // https://developers.quaderno.io/api/?php#deliver-an-credit .
      if (!empty($response_body['id']) && $this->config->get('enable_deliver_invoice') == TRUE) {
        $this->quaderno->deliverCredit($response_body['id']);
      }

    }
    $order->setData($this->pluginId, $order_data);
  }

  /**
   * Make transaction delete request to API.
   */
  public function deleteTransaction(OrderInterface $order) {
    $order_data = $order->getData($this->pluginId);

    if (empty($order_data['transactionInvoice']['id'])) {
      return;
    }

    // Delete refund if it exists.
    // https://developers.quaderno.io/api/?php#delete-a-credit .
    if ($order_data['transactionCredit']) {
      $this->quaderno->deleteCredit($order_data['transactionCredit']['id']);
    }

    // Delete order.
    // https://developers.quaderno.io/api/?php#delete-an-invoice .
    $this->quaderno->deleteInvoice($order_data['transactionInvoice']['id']);
  }

  /**
   * {@inheritdoc}
   */
  public function buildRequest(OrderInterface $order, $mode = 'quote') {
    $request_body = [
      'line_items' => [],
    ];

    $store = $order->getStore();
    $store_address = $store->getAddress();

    $prices_include_tax = $store->get('prices_include_tax')->value;
    $request_body['prices_include_tax'] = $prices_include_tax;
    $order_items_attributes = $order->getItems();
    $request_body['from_country'] = $store_address->getCountryCode();
    $request_body['from_postal_code'] = $store_address->getPostalCode();

    // Add line items.
    foreach ($order_items_attributes as $item) {
      if (!empty($item->getOrder())) {
        $profile = $this->resolveCustomerProfile($item);
        // If we could not resolve a profile for the order item, do not add it
        // to the API request. There may not be an address available yet, or the
        // item may not be shippable and not attached to a shipment.
        if (!$profile) {
          continue;
        }
        $profile_address = $profile->get('address')->first();
        $product = $item->getPurchasedEntity();
        $line_item = [
          'uuid' => $item->uuid(),
          'quantity' => $item->getQuantity(),
          'unit_price' => $item->getUnitPrice()->getNumber(),
          'amount' => $item->getAdjustedTotalPrice()
            ->getNumber() ?: $item->getTotalPrice()->getNumber(),
          'tax_behavior' => $prices_include_tax ? 'inclusive' : 'exclusive',
          'description' => $product->getTitle(),
          'reference' => method_exists($product, 'getSku') ? $product->getSku() : '',
          'to_country' => $profile_address->getCountryCode(),
          'to_postal_code' => $profile_address->getPostalCode(),
        ];

        if ($term = $item->getPurchasedEntity()->quaderno_category_code->entity) {
          $line_item['product_tax_code'] = $term->quaderno_category_code->value;
        }
        if ($profile->hasField('tax_number') && isset($profile->get('tax_number')->getValue()[0]['value'])) {
          $line_item['tax_id'] = $profile->get('tax_number')->getValue()[0]['value'];
        }

        if ($this->config->get('include_discounts') == TRUE) {
          $discount_rate = 0;
          $discount = 0;
          foreach ($item->getAdjustments() as $adjustment) {

            if ($adjustment->getType() === 'promotion') {
              if ($adjustment->getPercentage()) {
                $discount_rate = $adjustment->getPercentage() * 100;
              }
              else {
                $discount -= $adjustment->getAmount()->getNumber();
                $discount_rate = ($discount * 100) / $item->getUnitPrice()
                  ->getNumber();
              }

            }
          }
          if ($discount_rate !== 0) {
            $line_item['discount_rate'] = round($discount_rate);
            $line_item['amount'] = $item->getAdjustedTotalPrice()->getNumber();
            $line_item['discounted_unit_price'] = $item->getAdjustedTotalPrice([
              'promotion',
              'fee',
            ])->getNumber();
          }
        }

        $request_body['line_items'][] = $line_item;
      }
    }

    $has_shipments = $order->hasField('shipments') && !$order->get('shipments')
      ->isEmpty();
    if ($has_shipments) {
      /** @var \Drupal\commerce_shipping\Entity\ShipmentInterface $shipment */
      foreach ($order->get('shipments')->referencedEntities() as $shipment) {
        if (is_null($shipment->getAmount())) {
          continue;
        }
        $shipment_address = $shipment->getShippingProfile()
          ->get('address')
          ->first();
        $request_body['line_items'][] = [
          'uuid' => $shipment->uuid(),
          'description' => $shipment->getShippingMethod()
            ->label() ?: $shipment->label(),
          'reference' => $shipment->label(),
          'unit_price' => $shipment->getAmount()->getNumber(),
          'amount' => $shipment->getAmount()->getNumber(),
          'tax_behavior' => $prices_include_tax ? 'inclusive' : 'exclusive',
          "product_tax_code" => $this->config->get('shipping_tax_code'),
          'to_country' => $shipment_address->getCountryCode(),
          'to_postal_code' => $shipment_address->getPostalCode(),
        ];
      }
    }

    // Let other modules alter the request.
    $this->moduleHandler->alter('commerce_quaderno_tax_request', $request_body, $order);

    if ($mode === 'transaction') {

      $request_body['po_number'] = $order->getOrderNumber();
      $request_body['issue_date'] = DrupalDateTime::createFromTimestamp($order->getPlacedTime())
        ->format('Y-m-d');
      $request_body['currency'] = $order->getTotalPrice()->getCurrencyCode();
      $request_body['contact']['email'] = $order->getEmail();
      $billing_profile = $order->getBillingProfile();

      if ($billing_profile && $billing_profile->address) {
        $address = $billing_profile->address->first();
        $request_body['contact']['first_name'] = $address->getGivenName();
        $request_body['contact']['last_name'] = $address->getFamilyName();
        $request_body['contact']['contact_name'] = $address->getGivenName();
        $request_body['contact']['country'] = $address->getCountryCode();
        $request_body['contact']['postal_code'] = $address->getPostalCode();
        $request_body['contact']['region'] = $address->getAdministrativeArea();
        $request_body['contact']['city'] = $address->getLocality();
        $request_body['contact']['street_line_1'] = $address->getAddressLine1();
        // Concatenate street line 2 if supplied.
        if (!empty($address->getAddressLine2())) {
          $request_body['contact']['street_line_2'] = $address->getAddressLine2();
        }
        if ($billing_profile->hasField('field_quaderno_tax_id') && !$billing_profile->get('field_quaderno_tax_id')
          ->isEmpty()) {
          $request_body['contact']['tax_id'] = $billing_profile->get('field_quaderno_tax_id')->value;
        }

      }

      $order_data = $order->getData($this->pluginId);
      $items_attributes = [];
      foreach ($request_body['line_items'] as $item) {
        $items_attributes[$item['uuid']] = $item;

        if (!empty($order_data['taxResponse'])) {
          foreach ($order_data['taxResponse']['items_attributes'] as $response_item) {

            if ($items_attributes[$item['uuid']]['uuid'] == $response_item['uuid']) {
              if ($prices_include_tax) {

                $items_attributes[$item['uuid']]['total_amount'] = $items_attributes[$item['uuid']]['amount'];
                unset($items_attributes[$item['uuid']]['unit_price']);
              }
              $items_attributes[$item['uuid']]['tax_1_name'] = $response_item['name'];
              $items_attributes[$item['uuid']]['tax_1_rate'] = $response_item['rate'];
              $items_attributes[$item['uuid']]['tax_1_country'] = $response_item['country'];
              $items_attributes[$item['uuid']]['tax_1_region'] = $response_item['region'];
              $items_attributes[$item['uuid']]['tax_behavior'] = $response_item['tax_behavior'];
              $items_attributes[$item['uuid']]['tax_1_transaction_type'] = $response_item['tax_code'];
            }
          }
        }
      }

      $request_body['items_attributes'] = array_values($items_attributes);

      $request_body['payment_method'] = 'credit_card';
      $request_body['payment_processor'] = 'drupal_commerce';
      $request_body['payment_processor_id'] = $order->getOrderNumber();
      $request_body['custom_metadata'] = [
        'ip_address' => $this->requestStack->getCurrentRequest()->getClientIp(),
      ];
      // Let other modules alter the request.
      $this->moduleHandler->alter('commerce_quaderno_transaction_request', $request_body, $order);
    }

    return $request_body;
  }

}
