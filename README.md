CONTENTS OF THIS FILE
---------------------

* Introduction
* Requirements
* Installation
* Configuration
* Important Notes
* Credits
* Maintainers

INTRODUCTION
------------

The Commerce Quaderno module provides integration with the Quaderno
automated sales tax calculation and reporting for Drupal Commerce.

The tax is calculated based on the delivery address, the sales tax codes
assigned to line item in the order.

Access to a full development account can be created on
[sandbox-quadernoapp.com](https://sandbox-quadernoapp.com/login).

The service uses the [Quaderno Rest api](https://developers.quaderno.io/api/?php#invoices)
for processing transactions.

* For a full description of the module, visit the
  [project page on drupal.org](https://drupal.org/project/commerce_quaderno).

* To submit bug reports and feature suggestions, or to track changes:
  [drupal.org/project/issues/commerce_quaderno](https://drupal.org/project/issues/commerce_quaderno)

* For more information about Quaderno, visit the website: [quaderno.io](https://www.quaderno.io/)

REQUIREMENTS
------------

This module requires the following outside of Drupal core:

* Commerce - https://www.drupal.org/project/commerce


INSTALLATION
------------

 * Installation via composer is recommended, use:
```bash
composer require drupal/quaderno:^1.0
```

* Visit https://www.drupal.org/node/1897420 for further information.

CONFIGURATION
-------------

    1. Navigate to Administration > Extend and enable the module and its
       dependencies.
    2. Navigate to Administration > Commerce > Configuration > Quaderno settings
       for configuration settings.
    3. Uncheck "Disable tax calculation" (enabled by default)
    4. Select the mode to use when calculating taxes: Development or Production.
    5. Enter the API url, API private key, and API version.
    6. Save configuration.
    7. Navigate to Administration > Commerce > Configuration > Stores
    8. Click edit on your own store
    9. Enable "Prices are entered with taxes included." for inclusive or disable exclusive tax.
    10. Go to your product and select the appropriate tax code from the Quaderno Category Code field
    11. Navigate to Administration > Configuration > People > Profile Types > Edit Customer
    12. Enable the Tax Number(tax_number) field in both Manage form display and Manage display tabs

 * Once the new tax type is saved, the module will automatically fetch and download the available
   product tax categories from the Quaderno service. These will be saved in the "Quaderno Categories"
   taxonomy vocabulary, and can be referenced from any commerce product variations which fall under
   a special tax category.
 * Shipping tax code will be available and can be changed from Quaderno tax type once the
   tax codes are synced.
   Navigate to Administration > Commerce > Configuration > Tax Types and select from Shipping tax codes.


IMPORTANT NOTES
---------------
In Drupal Commerce there are 4 scenarios on how you can display tax. Currently, this module
only supports the following scenarios:

1. Prices are NOT entered with taxes included
   On checkout review the following is displayed:
   Subtotal $100.00
   Tax $20.00
   Total $120.00

2. Prices are entered with taxes included
   On checkout review the following is displayed:
   Subtotal $120.00
   Tax $20.00
   Total $120.00

On scenario 2 (Prices are entered with taxes included) discounts will be calculated on the
full prices rather then the unit price. The Quaderno invoice will generate the correct discounted
price. For more information visit https://www.drupal.org/project/commerce/issues/2982355#comment-13997358

CREDITS
-------------

This module was inspired from the following similar tax modules:
* Commerce TaxJar - https://www.drupal.org/project/commerce_taxjar
* Commerce Avatax - https://www.drupal.org/project/commerce_avatax

A big thank you to the developers who designed the modules.

MAINTAINERS
-----------

* lexsoft - https://www.drupal.org/u/lexsoft
