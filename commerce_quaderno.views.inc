<?php

/**
 * @file
 * Provide views data for commerce_quaderno.module.
 *
 * @ingroup views_module_handlers
 */

/**
 * Implements hook_views_data().
 */
function commerce_quaderno_views_data() {
  $data['views']['quaderno_invoice_links'] = [
    'title' => t('Quaderno Invoice Links'),
    'help' => t('Displays the Quaderno invoice links.'),
    'field' => [
      'id' => 'quaderno_invoice_links',
    ],
  ];

  return $data;
}
