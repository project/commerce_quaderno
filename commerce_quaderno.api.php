<?php

/**
 * @file
 * Defines hooks made available by commerce_quaderno.
 */

use Drupal\commerce_order\Entity\OrderInterface;

/**
 * Allow alter the tax quote request before it is sent to the Quaderno API.
 *
 * @param array $request_body
 *   The request body array.
 * @param \Drupal\commerce_order\Entity\OrderInterface $order
 *   The order object.
 */
function hook_commerce_quaderno_tax_request_alter(array &$request_body, OrderInterface $order) {
}

/**
 * Allows alter the transaction request before its sent to the Quaderno API.
 *
 * @param array $request_body
 *   The request body array.
 * @param \Drupal\commerce_order\Entity\OrderInterface $order
 *   The order object.
 */
function hook_commerce_quaderno_transaction_request_alter(array &$request_body, OrderInterface $order) {
}
